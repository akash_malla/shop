package se.par.amsen.shopdesign.service.shopdata;

import java.util.List;

import se.par.amsen.shopdesign.domain.Category;
import android.content.Context;

public interface ShopDataService {
	public List<Category> getShopData(Context context);
}
